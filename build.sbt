name := "otp"

organization := "com.filez.scala.kuro"

version := "1.1.0"

ThisBuild / scalaVersion := crossScalaVersions.value.head

crossScalaVersions := Seq("2.12.17", "2.11.12", "2.13.10", "3.1.3")

scalacOptions ++= {
  Seq(
    "-encoding", "UTF-8",
    "-deprecation",
    "-unchecked",
    "-feature",
    "-explaintypes",
    "-Xfatal-warnings"
  )  ++ (scalaVersion.value.split('.').map(_.toInt).toList match {
    case v @ 2 :: 11 :: _ =>
      Seq("-optimise")
    case v @ 2 :: major :: _ if major >= 12 =>
      Seq("-opt:l:method", "-Ywarn-extra-implicit")
    case v =>
      Nil
  }) ++ (scalaVersion.value.split('.').map(_.toInt).toList match {
    case v @ 2 :: major :: _ if major < 13 =>
      Seq("-Ywarn-inaccessible", "-Ywarn-infer-any", "-Ywarn-nullary-override", "-Ywarn-nullary-unit",
          "-Ywarn-unused-import", "-target:jvm-1.8"
         )
    case 2 :: 13 :: _ | 3 :: _ :: _ => Seq ("-release:8" )
    case v =>
      Nil
  }) ++ (scalaVersion.value.split('.').map(_.toInt).toList match {
    case 2 :: _ :: _ => Seq("-Xlint", "-Ywarn-dead-code", "-Ywarn-numeric-widen", "-Ywarn-value-discard")
    case v =>
      Nil
  })
}

Test / publishArtifact := true

publishTo := sonatypePublishToBundle.value

ThisBuild / versionScheme := Some("early-semver")

Compile / console / scalacOptions := Nil

Compile / doc / scalacOptions ++= { Seq(
  "-sourcepath", baseDirectory.value.getAbsolutePath,
  "-doc-title", "Kuro OTP (HOTP, TOTP)",
  "-doc-footer", "Copyright (c) 2017 Ryo Ejima (ejisan), Apache License v2.0." 
  ) ++ (scalaVersion.value.split('.').map(_.toInt).toList match {
    case 2 :: _ :: _ =>
      Seq( "-doc-source-url", "https://gitlab.com/hsn10/scala-otp/-/blob/master€{FILE_PATH}.scala")
    case 3 :: _ :: _ =>
      Seq( "-source-links:src=gitlab://hsn10/scala-otp/master#src")
    case _ => Nil
  })
}

javacOptions ++= Seq("-source", "1.8")

Test / testOptions ++= Seq(
  Tests.Argument(TestFrameworks.ScalaTest, "-oD"),
  Tests.Argument(TestFrameworks.JUnit, "-q", "-v"))

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.4.2",
  "org.scala-lang.modules" %% "scala-java8-compat" % "1.0.2",
  "org.scala-lang.modules" %% "scala-collection-compat" % "2.9.0",
  "commons-codec" % "commons-codec" % "1.15",
  "junit" % "junit" % "4.13.2" % Test,
  "com.geirsson" % "junit-interface" % "0.11.11" % Test,
  "org.scalatest" %% "scalatest" % "3.2.15" % Test)
