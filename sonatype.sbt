// Maven Central OSS Release train:
///// run these sbt commands:
///// +publishSigned
///// sonatypePrepare
///// +sonatypeBundleUpload
///// go to https://oss.sonatype.org/#stagingRepositories and review what got uploaded
///// happy? sonatypeRelease

Global / useGpg := false

sonatypeProfileName := "com.filez"

pomExtra in Global := {
  <url>https://github.com/hsn10/scala-otp</url>
  <licenses>
    <license>
      <name>Apache-2.0</name>
      <url>http://www.apache.org/licenses/LICENSE-2.0.html</url>
      <distribution>repo</distribution>
    </license>
  </licenses>
  <scm>
    <connection>scm:git:https://gitlab.com/hsn10/scala-otp.git</connection>
    <developerConnection>scm:git:git@gitlab.com:hsn10/scala-otp.git</developerConnection>
    <url>https://gitlab.com/hsn10/scala-otp</url>
  </scm>
  <developers>
    <developer>
      <id>ejisan</id>
      <name>Ryo Ejima</name>
    </developer>
    <developer>
      <id>hsn10</id>
      <name>Radim Kolar</name>
    </developer>
  </developers>
}
