/*
Copyright 2017 Ryo Ejima, 2023 Radim Kolar

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.filez.scala.kuro.otp;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class OTPAlgorithmTest {

  @Test
  public void testAlgorithmValue() throws Exception {
    assertThat(OTPAlgorithm.getMD5().value(), is ("HmacMD5"));
    assertThat(OTPAlgorithm.getSHA1().value(), is ("HmacSHA1"));
    assertThat(OTPAlgorithm.getSHA256().value(), is ("HmacSHA256"));
    assertThat(OTPAlgorithm.getSHA512().value(), is ("HmacSHA512"));
  }

  @Test
  public void testAlgorithmName() throws Exception {
    assertThat(OTPAlgorithm.getMD5().name(), is ("MD5"));
    assertThat(OTPAlgorithm.getSHA1().name(), is ("SHA1"));
    assertThat(OTPAlgorithm.getSHA256().name(), is ("SHA256"));
    assertThat(OTPAlgorithm.getSHA512().name(), is ("SHA512"));
  }
}
