/*
Copyright 2017 Ryo Ejima, 2023 Radim Kolar

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.filez.scala.kuro.otp

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

class OTPAlgorithmSpec extends AnyFlatSpec with Matchers {

  // https://github.com/google/google-authenticator/wiki/Key-Uri-Format
  "OTPAlgorithm#name" should "return algorithm value for otpauth URI" in {
    OTPAlgorithm.MD5.name should be ("MD5")
    OTPAlgorithm.SHA1.name should be ("SHA1")
    OTPAlgorithm.SHA256.name should be ("SHA256")
    OTPAlgorithm.SHA512.name should be ("SHA512")
  }

  "OTPAlgorithm#value" should "return algorithm value for Java [[javax.crypto.Mac]]" in {
    OTPAlgorithm.MD5.value should be ("HmacMD5")
    OTPAlgorithm.SHA1.value should be ("HmacSHA1")
    OTPAlgorithm.SHA256.value should be ("HmacSHA256")
    OTPAlgorithm.SHA512.value should be ("HmacSHA512")
  }

  "OTPAlgorithm.unapply" should "extract algorithm name and value" in {
    OTPAlgorithm.unapply(OTPAlgorithm.SHA1) should be (Some("SHA1" -> "HmacSHA1"))
  }
}
