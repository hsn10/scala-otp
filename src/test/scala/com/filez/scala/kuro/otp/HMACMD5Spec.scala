/*
Copyright 2017 Ryo Ejima, 2023 Radim Kolar

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package com.filez.scala.kuro.otp

import org.scalatest.matchers.should._
import org.scalatest.flatspec.AnyFlatSpec

import javax.crypto.Mac
import org.apache.commons.codec.binary.Hex

class HMACMD5Spec extends AnyFlatSpec with Matchers {

   behavior of "HMAC-MD5"

       "rfc2104 Test Vector 1" should "validate" in {
          val key1 = OTPKey.fromHex("0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b", false)
          key1.get.getEncoded().length should be (16)
          val data = "Hi There"
          data.length() should be (8)
          val hmac = Mac.getInstance("HmacMD5")
          hmac.init(key1.get)
          val result = hmac.doFinal(data.getBytes("UTF-8"))
          val digest = "9294727a3638bb1c13f48ef8158bfc9d"
          result should be (Hex.decodeHex(digest))
       }

       "rfc2104 Test Vector 2" should "validate" in {
          val key2 = OTPKey.fromByteArray("Jefe".getBytes("UTF-8"), false)
          val data = "what do ya want for nothing?"
          data.length() should be (28)
          val digest = "750c783e6ab0b503eaa86e310a5db738"
          val hmac = Mac.getInstance("HmacMD5")
          hmac.init(key2.get)
          val result = hmac.doFinal(data.getBytes("UTF-8"))
          result should be (Hex.decodeHex(digest))
       }

       "rfc2104 Test Vector 3" should "validate" in {
          val key3 = OTPKey.fromHex("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
          key3.get.getEncoded().length should be (16)
          val data = Array.fill[Byte](50)(0xDD.toByte)
          data.length should be (50)
          val digest = "56be34521d144c88dbb8c733f0e8b3f6"
          val hmac = Mac.getInstance("HmacMD5")
          hmac.init(key3.get)
          val result = hmac.doFinal(data)
          result should be (Hex.decodeHex(digest))
       }
}
